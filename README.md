Project Name - API testing

* What is API?

API stands for Application Programming Interface. It's a set of definitions, protocols, and tools for building application software.

Tool used for Api testing : Postman

* Postman Tool :

Postman is a popular tool used by developers for building, testing, and modifying APIs.
Designed as a Chrome browser extension, it has evolved into a comprehensive, standalone application that supports various systems, including Windows, macOS, and Linux.

* Methods for Testing Api in Postman are as follows :

1. Creating Request
1.1 We can use post , patch, put, get, delete, get, methods

2. Writing Test
2.1 Test are written in javascript.We used the "pm" object to interact with the request and respons.

3.Running Test
3.1 we can run them by sending the request.

4. Variables and Environmnet
4.1 We can enhance your tests by using variables and environments in Postman. 
This allows us to easily switch between different sets of data or server configurations without changing your tests. 

5. Collection Runner
5.1 It is used to run a series of request

* Data Driven Testing

Data-driven testing in Postman involves executing a collection of requests/tests multiple times with different sets of data.
For this I created cvs files and json file.

* Newman Report

Newman is a command-line Collection Runner for Postman. It allows you to run and test a Postman collection directly from the command line. 
It's built on Node.js and can be easily integrated into continuous integration (CI) servers and build systems, making it an invaluable tool for automated testing and deployment pipelines. 
Newman maintains feature parity with Postman and allows to run collections exactly the way they are executed inside Postman. 





